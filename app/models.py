from django.db import models

# Pestaña models, se crean las tablas para su posterior migración (comando py manage.py makemigrations)


class Clientes(models.Model):
    id_cliente = models.IntegerField()
    rut = models.CharField(max_length=50)
    nombre_compania = models.CharField(max_length=50)
    telefono = models.IntegerField()
    email = models.CharField(max_length=50)
    direccion = models.CharField(max_length=50, default='')

    def __str__(self):
        return self.nombre_compania


#estado_nv = (
#    (0,'Sin Seleccionar'),
#    (1,'Aprobado'),
#    (2,'En Progreso'),
#    (3,'Abortada'),
#    (4,'Finalizada'),
#    (5,'Eliminado'),
#)




clase_venta = [

    [0, "Contado"],
    [1, "Credito"]
]


class N_venta(models.Model):
    id_cliente = models.ForeignKey(Clientes, on_delete=models.PROTECT)
    nombre_venta = models.CharField(max_length=50)
    fecha_venta = models.DateField()
    clp = models.IntegerField()
    clase_venta = models.IntegerField(choices=clase_venta)
    fecha_inicio_facturacion = models.DateField()
    fecha_fin_facturacion = models.DateField()
    #estado_nota_venta = models.IntegerField(choices=estado_nv, default=0)
    def __str__(self):
        return self.nombre_venta


