from app.filters import OrderFilter
import xlwt
from app.models import Clientes, N_venta
from django.shortcuts import render, redirect, get_object_or_404
from .forms import ClienteForm, NventaForm, CustomUserCreationForm,ModClienteForm,ModNventasForm
from django.http import HttpResponse
from django.contrib import messages
from django.contrib.auth import authenticate, login as django_login
from django.contrib.auth.decorators import login_required, permission_required
from django.db.models import Q
from .filters import OrderFilter, OrderFilter1
# Create your views here.




#-------------------------------------
# INDEX
#-------------------------------------
@login_required
def index(request):
    return render(request, 'app/index.html')

#-------------------------------------
# ADMINISTRAR O AGREGAR CLIENTES
#-------------------------------------
@login_required
def adm_clientes(request):

    data = {

        'form': ClienteForm()

    }

    if request.method == 'POST':
        formulario = ClienteForm(data=request.POST, files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            messages.success(request, "Guardado correctamente")
            return redirect(to="adm_clientes")
        data["form"] =formulario

    return render(request, 'app/adm_clientes.html',data) 

#-------------------------------------
# ADMINISTRAR USUARIOS
#-------------------------------------
@login_required
def adm_usuarios(request):
    return render(request, 'app/adm_usuarios.html')

#-------------------------------------
# TIPO DE USUARIO PARA INGRESAR A SISTEMA
#-------------------------------------
def loginopcion(request):
    return render(request, 'app/loginopcion.html')

#-------------------------------------
# ADMINISTRAR O AGREGAR NOTA DE VENTA
#-------------------------------------
@login_required
def adm_ventas(request):

    data = {

        'form': NventaForm()
    }

    if request.method == 'POST':
        formulario = NventaForm(data=request.POST, files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            messages.success(request, "Guardado correctamente")
            return redirect(to="adm_ventas")
        data["form"] =formulario

    return render(request,'app/adm_nota_venta.html',data)

#-------------------------------------
# LOGIN
#-------------------------------------
@login_required
def login(request):
    return render(request, 'app/login.html')

#-----------------------
# LISTAR CLIENTES
#-----------------------
@login_required
def listar_clientes(request):
    clientes = Clientes.objects.all()

    #Filtar en tabla por nombre_compania
    myFilter = OrderFilter(request.GET, queryset=clientes)
    clientes = myFilter.qs 

    data = {
        'clientes': clientes,
        'myfilter' : myFilter
    }

    return render(request, 'app/listar_clientes.html',data)

#-----------------------
# LISTAR NOTA DE VENTA
#-----------------------
@login_required
def listar_ventas(request):
    ventas = N_venta.objects.all()

    #Filtar en tabla por nombre_compania
    myFilter1 = OrderFilter1(request.GET, queryset=ventas)
    ventas = myFilter1.qs 

    data = {
        'ventas': ventas,
        'myfilter1' : myFilter1
     }
    return render(request,'app/listar_ventas.html',data)

#-----------------------
# MODIFICAR NOTA DE VENTA
#-----------------------
@login_required
def modificar_venta(request, id):

    ventas = get_object_or_404(N_venta, id=id)

    data = {
        'form': ModNventasForm(instance=ventas)
    }
     
    if request.method == 'POST':
        formulario = ModNventasForm(data=request.POST,instance=ventas, files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            messages.success(request, "Modificado correctamente")
            return redirect(to="listar_ventas")
       
        data["form"] =formulario 
    return render(request, 'app/modificar_venta.html',data)

#-----------------------
# ELIMINAR NOTA DE VENTA
#-----------------------
@login_required
def eliminar_venta(request,id):
    ventas = get_object_or_404(N_venta, id=id)
    ventas.delete()
    messages.success(request, "Eliminado correctamente")
    return redirect(to = "listar_ventas")


#-------------------
# MODIFICAR CLIENTE
#-------------------
@login_required
def modificar_clientes(request, id):

    clientes = get_object_or_404(Clientes, id=id)

    data = {
        'form': ModClienteForm(instance=clientes)
    }
     
    if request.method == 'POST':
        formulario = ModClienteForm(data=request.POST,instance=clientes, files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            messages.success(request, "Modificado correctamente")
            return redirect(to="listar_clientes")
        data["form"] = formulario 

    return render(request, 'app/modificar_clientes.html',data)  

#-------------------------------------
# ELIMINAR CLIENTES
#-------------------------------------
@login_required
def eliminar_cliente(request,id):
    clientes = get_object_or_404(Clientes, id=id)
    clientes.delete()
    messages.success(request, "Eliminado correctamente")
    return redirect(to = "listar_clientes")      

#-------------------------------------
# EXPORTAR INFORMACIÓN CLIENTES
#-------------------------------------
def export_clientes_xls(request):

    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="Clientes.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Clientes')

    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = ['id_cliente', 'rut', 'nombre_compania', 'telefono', 'email','direccion' ]

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    rows = Clientes.objects.all().values_list('id_cliente', 'rut', 'nombre_compania', 'telefono','email','direccion')
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response

#-------------------------------------
# EXPORTAR INFORMACIÓN CLIENTES
#-------------------------------------
def export_nota_venta_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="NotaVenta.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('N_venta')

    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = ['id_cliente', 'nombre_venta', 'fecha_venta', 'clp', 'clase_venta','fecha_inicio_facturacion','fecha_fin_facturacion' ]

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    rows = N_venta.objects.all().values_list('id_cliente', 'nombre_venta', 'fecha_venta', 'clp', 'clase_venta','fecha_inicio_facturacion','fecha_fin_facturacion')
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response

#-------------------------------------
# REPORTE
#-------------------------------------
@login_required
def reporte(request):
 return render(request, 'app/reporte.html')

#-------------------------------------
# REGISTRO DE USUARIOS
#-------------------------------------
@login_required
def registro(request):
    data = {
        'form': CustomUserCreationForm()
    }

    if request.method == 'POST':
        formulario = CustomUserCreationForm(data=request.POST)
        if formulario.is_valid():
            formulario.save()
            user = authenticate(username=formulario.cleaned_data["username"], password=formulario.cleaned_data["password1"])
            messages.success(request,"Usuario Registrado.")
            return redirect(to = "index")
        data["form"]= formulario

    return render(request, 'registration/registro.html', data)