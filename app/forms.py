from django import forms
from django.forms import widgets
from .models import Clientes, N_venta
from django.forms import ValidationError
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class DateInput(forms.DateInput):
    input_type = 'date'

# Formularios de Modificación de Clientes


class ModClienteForm (forms.ModelForm):
    class Meta:
        model = Clientes
        fields = '__all__'
        widgets = {
            "id_cliente": widgets.TextInput(attrs={'readonly':'readonly'}),
            "rut": widgets.TextInput(attrs={'readonly':'readonly'})
        }

# Formularios de Modificación de Nota de Ventas


class ModNventasForm (forms.ModelForm):
    class Meta:
        model = N_venta
        fields = '__all__'
        widgets = {
            "id_cliente": widgets.TextInput(attrs={'readonly':'readonly'}),
            
        }


# formularios de guardado Cliente
class ClienteForm(forms.ModelForm):

    def clean_id_cliente(self):
        id_cliente = self.cleaned_data["id_cliente"]
        existe = Clientes.objects.filter(id_cliente=id_cliente).exists()

        if existe:
            raise ValidationError("ID de cliente ya existe")

        return id_cliente

    def clean_rut(self):
        rut = self.cleaned_data["rut"]
        existe = Clientes.objects.filter(rut=rut).exists()

        if existe:
            raise ValidationError("Rut de cliente ya registrado")

        return rut

    def clean_nombre_compañia(self):
        nombre_compañia = self.cleaned_data["nombre_compañia"]
        existe = Clientes.objects.filter(
            nombre_compañia=nombre_compañia).exists()

        if existe:
            raise ValidationError("Nombre de compañia ya registrado")

        return nombre_compañia

    def clean_telefono(self):
        telefono = self.cleaned_data["telefono"]
        existe = Clientes.objects.filter(telefono=telefono).exists()

        if existe:
            raise ValidationError("Telefono ya registrado")

        return telefono

    def clean_email(self):
        email = self.cleaned_data["email"]
        existe = Clientes.objects.filter(email=email).exists()

        if existe:
            raise ValidationError("Email ya registrado")

        return email

    class Meta:
        model = Clientes
        fields = '__all__'

# formularios de guardado Nota de Venta
class NventaForm(forms.ModelForm):

    def clean_nombre_venta(self):
        nombre_venta = self.cleaned_data["nombre_venta"]
        existe = N_venta.objects.filter(nombre_venta=nombre_venta).exists()

        if existe:
            raise ValidationError("Venta ya registrada")

        return nombre_venta

    class Meta:
        model = N_venta
        fields = '__all__'

        widgets = {
            "fecha_venta": DateInput(),
            "fecha_inicio_facturacion": DateInput(),
            "fecha_fin_facturacion": DateInput(),


        }

    # Formulario de registro


class CustomUserCreationForm(UserCreationForm):
    def save(self, commit = True):
        user = super().save()
        self.save_m2m()
        return user
    class Meta:
        model = User
        fields = ['username', "first_name", "last_name",
                  "email", "password1", "password2",'groups']
