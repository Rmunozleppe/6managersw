import django_filters
from django_filters import DateFilter, CharFilter

from .models import *


class OrderFilter(django_filters.FilterSet):
	nombre_compania = CharFilter(field_name='nombre_compania', lookup_expr='icontains')


	class Meta:
		model = Clientes
		fields = '__all__'
		exclude = ['id_cliente', 'rut','telefono','email','direccion']

class OrderFilter1(django_filters.FilterSet):

    

	class Meta:
		model = N_venta
		fields = '__all__'
		exclude = ['nombre_venta', 'fecha_venta','clp','clase_venta','fecha_inicio_facturacion','fecha_fin_facturacion']        


