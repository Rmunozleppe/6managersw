from django.urls import path, include
from .views import eliminar_cliente, index, adm_clientes, adm_usuarios, listar_clientes, loginopcion, adm_ventas, listar_ventas, modificar_clientes,modificar_venta, eliminar_venta,export_clientes_xls,export_nota_venta_xls,  registro, reporte
urlpatterns = [
    
    path('jet/', include('jet.urls','jet')),
    path('index/', index , name="index"),
    path('adm_clientes/', adm_clientes, name="adm_clientes"),
    path('adm_usuarios/', adm_usuarios, name="adm_usuarios"),
    path('', loginopcion, name="loginopcion"),
    path('adm_venta/',adm_ventas, name="adm_ventas"),
    path('listar_clientes/',listar_clientes, name="listar_clientes"),
    path('listar_ventas/',listar_ventas, name="listar_ventas"),
    path('modificar_venta/<id>/',modificar_venta,name="modificar_venta"),
    path('eliminar_venta/<id>',eliminar_venta,name='eliminar_venta'),
    path('modificar_clientes/<id>',modificar_clientes,name="modificar_clientes"),
    path('eliminar_cliente/<id>',eliminar_cliente,name="eliminar_cliente"),
    path('export/xls/',export_clientes_xls, name='export_clientes_xls'),
    path('export/xlss/',export_nota_venta_xls, name='export_nota_venta_xls'),
    path('registro/',registro,name="registro"),
    path('reporte/',reporte,name="reporte"),
]